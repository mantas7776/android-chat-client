package com.mantas.chat;

import android.app.Application;

import com.mantas.chat.chatroom.ChatModule;
import com.mantas.chat.user.events.ValidatedSession;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MyApplication extends Application {
    private AppComponent appComponent;
    private WebSocketClient ws;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent
                .builder()
                .chatModule(new ChatModule(this))
                .build();

        appComponent.getEventBus().register(this);
        ws = new WebSocketClient(appComponent.getEventBus(), appComponent.getOkHttpClient());
        appComponent.getUserRepository().login();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onValidatedSession(ValidatedSession v) {
        appComponent.getChatRepository().load();
        ws.run();
    }
}
