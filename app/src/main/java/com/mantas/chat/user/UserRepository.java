package com.mantas.chat.user;

import android.provider.CallLog;
import android.util.Log;

import com.mantas.chat.user.events.ValidatedSession;
import com.mantas.chat.user.webResponses.GetMyInfoResponse;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UserRepository {
    private static final String TAG = "UserRepository";
    private Retrofit retrofit;
    private UserWebservice webservice;
    private EventBus eventBus;
    @Inject
    public UserRepository(Retrofit retrofit, EventBus eventBus) {
        this.retrofit = retrofit;
        this.webservice = retrofit.create(UserWebservice.class);
        this.eventBus = eventBus;
    }

    public void login() {
        Call<GetMyInfoResponse> call = webservice.getMyInfo();
        call.enqueue(new Callback<GetMyInfoResponse>() {
            @Override
            public void onResponse(Call<GetMyInfoResponse> call, Response<GetMyInfoResponse> response) {
                if (!response.body().isSuccess()) {
                    createUser();
                    return;
                }
                Log.d(TAG, "loginIfUnloggedSync: skipping user creation, we are already logged in with valid cookie!");
                eventBus.post(new ValidatedSession());
            }

            @Override
            public void onFailure(Call<GetMyInfoResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    private void createUser() {
        Call<Void> call = webservice.createUser();
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                eventBus.post(new ValidatedSession());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }
}
