package com.mantas.chat.user;

public class User {
    private int id;
    private String color;

    public User(int id, String color) {
        this.id = id;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getColor() {
        return color;
    }
}
