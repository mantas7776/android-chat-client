package com.mantas.chat.user.webResponses;

import com.mantas.chat.user.User;

public class GetMyInfoResponse {
    private boolean success;
    User user;

    public boolean isSuccess() {
        return success;
    }

    public User getUser() {
        return user;
    }
}
