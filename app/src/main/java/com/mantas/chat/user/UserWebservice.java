package com.mantas.chat.user;

import com.mantas.chat.user.webResponses.GetMyInfoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserWebservice {

    @POST("users/create")
    Call<Void> createUser();

    @GET("users/me")
    Call<GetMyInfoResponse> getMyInfo();
}
