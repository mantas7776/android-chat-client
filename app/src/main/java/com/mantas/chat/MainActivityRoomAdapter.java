package com.mantas.chat;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mantas.chat.chatroom.ChatRoom;
import com.mantas.chat.chatroom.ChatRoomActivity;
import com.mantas.chat.chatroom.ChatRoomSettings;

import java.util.List;

import static com.mantas.chat.MainActivity.CHATROOM;

public class MainActivityRoomAdapter extends RecyclerView.Adapter<MainActivityRoomAdapter.ViewHolder> {

    private static final String TAG = "MainActivityRoomAdapter";

    private List<ChatRoom> rooms;
    private Context mContext;

    public MainActivityRoomAdapter(List<ChatRoom> rooms, Context mContext) {
        this.rooms = rooms;
        this.mContext = mContext;
    }

    public void updateData(List<ChatRoom> rooms) {
        this.rooms = rooms;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_list_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        Log.d(TAG, "onBindView: created");

        holder.roomName.setText(rooms.get(i).getName());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ChatRoomActivity.class);
                intent.putExtra(CHATROOM, rooms.get(i).getName());
                mContext.startActivity(intent);
            }
        });

        holder.parentLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(mContext, ChatRoomSettings.class);
                intent.putExtra(CHATROOM, rooms.get(i).getName());
                mContext.startActivity(intent);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return rooms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout parentLayout;
        TextView roomName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            roomName = itemView.findViewById(R.id.chat_room_name);
            parentLayout = itemView.findViewById(R.id.parent_chat_item_layout);
        }
    }

}
