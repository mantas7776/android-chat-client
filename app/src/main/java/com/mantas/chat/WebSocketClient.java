package com.mantas.chat;

import android.util.JsonWriter;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mantas.chat.chatroom.Message;
import com.mantas.chat.chatroom.events.SendMessageEvent;
import com.mantas.chat.user.User;
import com.mantas.chat.chatroom.events.ReceivedMessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

public class WebSocketClient extends WebSocketListener {
    private static final String TAG = "WebSocketClient";
    private WebSocket ws;
    private EventBus eventBus;
    private OkHttpClient okHttpClient;

    public WebSocketClient(EventBus eventBus, OkHttpClient okHttpClient) {
        this.eventBus = eventBus;
        this.okHttpClient = okHttpClient;
        eventBus.register(this);
    }

    public void run() {
        Log.d(TAG, "run: Starting ws.");

        Request request = new Request.Builder()
                .url("ws://10.0.2.2:13337")
                .build();
        if (ws != null) ws.close(0, "disconnect");
        ws = okHttpClient.newWebSocket(request, this);

        //for clean process exit
        //okHttpClient.dispatcher().executorService().shutdown();
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        Log.d(TAG, "onOpen: Websocket connection opened.");
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        Log.w(TAG, "onFailure: ", t);
        t.printStackTrace();
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        Log.d(TAG, "onMessage: New ws message: " + text);
        Gson gson = new Gson();

        JsonObject json = gson.fromJson(text, JsonObject.class);

        if (json.get("type").getAsString().equals("newMessage"))
            handleNewMessage(json);
    }

    @Override public void onMessage(WebSocket webSocket, ByteString bytes) {
        Log.d(TAG, "onMessage: Received bytes, but there is no method to handle them!");
    }

    public void handleNewMessage(JsonObject json) {
        JsonObject userJson = json.get("user").getAsJsonObject();
        User user = new User(userJson.get("id").getAsInt(),userJson.get("color").getAsString());
        Message message = new Message(user, json.get("text").getAsString());
        String roomName = json.get("room").getAsString();
        Log.d(TAG, "handleNewMessage: received new message with text: " + message.getText());
        eventBus.post(new ReceivedMessageEvent(roomName, message));
    }

    @Subscribe
    public void sendNewMessage(SendMessageEvent data) {
        JsonObject o = new JsonObject();
        o.addProperty("type", "sendMessage");
        o.addProperty("room", data.roomName);
        o.addProperty("text", data.message);
        ws.send(o.toString());
    }
}
