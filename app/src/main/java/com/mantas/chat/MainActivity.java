package com.mantas.chat;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatDrawableManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mantas.chat.chatroom.ChatRoomActivity;
import com.mantas.chat.chatroom.ChatRoomJoinActivity;
import com.mantas.chat.chatroom.ChatRoomListViewModel;
import com.mantas.chat.dependencyInjection.ViewModelFactory;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static final String CHATROOM = "com.mantas.chat.CHATROOM";

    private ChatRoomListViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Chat rooms");


        Log.d(TAG, "onCreate: started.");

        //get viewmodel
        ViewModelFactory viewModelFactory = ((MyApplication) getApplication()).getAppComponent().getViewModelFactory();
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ChatRoomListViewModel.class);

        loadTestData();

        //set initial view
        RecyclerView recyclerView = findViewById(R.id.chat_rooms_list_recycler_view);
        Log.d(TAG, "onCreate: getting joined rooms");
        MainActivityRoomAdapter adapter = new MainActivityRoomAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //wait for data
        viewModel.getJoinedRooms().observe(this, rooms -> {
            Log.d(TAG, "onCreate: observer update, updating adapter data.");
            adapter.updateData(rooms);
        });


    }

    public void loadRoom(View view) {
        Log.d(TAG, "loadRoom: starting activity.");
        //rooms.add(new ChatRoom("Testxd", -1));
        Intent intent = new Intent(this, ChatRoomActivity.class);
        TextView textView = findViewById(R.id.chat_room_name);
        Log.d(TAG, "loadRoom: loading " + textView.getText().toString());
        intent.putExtra(CHATROOM, textView.getText().toString());

        startActivity(intent);
    }

    private void loadTestData() {
        Log.d(TAG, "loadTestData: adding test data.");
        //viewModel.joinRoom("Test");
//        ChatRoom test = new ChatRoom("Test", -1);
//        test.addMessage("hello", 0);
//        test.addMessage("msg2", 0x005F00);
//        //chatRepo.set(test);
        //chatRepo.joinRoom("Test");
    }

    public void showJoinRoomUI(View view) {
        Intent intent = new Intent(this, ChatRoomJoinActivity.class);
        startActivity(intent);
    }
}
