package com.mantas.chat.chatroom;

import com.mantas.chat.user.User;

public class Message {
    private User user;
    private String text;

    public Message(User user, String text) {
        this.user = user;
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }
}
