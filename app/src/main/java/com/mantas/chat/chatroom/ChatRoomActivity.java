package com.mantas.chat.chatroom;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.mantas.chat.AppComponent;
import com.mantas.chat.MainActivity;
import com.mantas.chat.MyApplication;
import com.mantas.chat.R;
import com.mantas.chat.dependencyInjection.ViewModelFactory;

public class ChatRoomActivity extends AppCompatActivity {


    private ViewModelFactory viewModelFactory;
    private ChatRoomViewModel viewModel;
    private EditText ourMessageBody;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        Intent intent = getIntent();
        String chatRoomName = intent.getStringExtra(MainActivity.CHATROOM);

        AppComponent appComponent = ((MyApplication) getApplication()).getAppComponent();
        viewModelFactory = appComponent.getViewModelFactory();
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ChatRoomViewModel.class);
        viewModel.init(chatRoomName);

        ourMessageBody = findViewById(R.id.our_message_text);

        setTitle(chatRoomName);

        RecyclerView recyclerView = findViewById(R.id.chat_room_recycler_view);
        ChatRoomAdapter adapter = new ChatRoomAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        LiveData<ChatRoom> room = viewModel.getRoom();
        room.observe(this, updatedRoom -> {
            adapter.setChatRoom(updatedRoom);
        });
    }

    public void sendMessage(View view) {
        String message = ourMessageBody.getText().toString();
        if (message.length() > 0) {
            viewModel.sendMessage(message);
            ourMessageBody.getText().clear();
        }
    }
}
