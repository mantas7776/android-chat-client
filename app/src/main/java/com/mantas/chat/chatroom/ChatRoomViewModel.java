package com.mantas.chat.chatroom;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

public class ChatRoomViewModel extends ViewModel {
    private static final String TAG = "ChatRoomViewModel";
    private ChatRoomRepository repo;
    private String roomName;
    private LiveData<ChatRoom> room;

    @Inject
    public ChatRoomViewModel(ChatRoomRepository repo) {
        this.repo = repo;
    }


    public void init(String roomName) {
        if (room != null) return;
        Log.d(TAG, "init: assigning room name and getting room data.");
        this.roomName = roomName;
        this.room = repo.getRoom(roomName);
    }

    public LiveData<ChatRoom> getRoom() {
        return room;
    }

    public void sendMessage(String messageText) {
        repo.sendMessage(roomName, messageText);
    }
}
