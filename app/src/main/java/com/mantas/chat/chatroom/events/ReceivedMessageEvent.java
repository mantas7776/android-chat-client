package com.mantas.chat.chatroom.events;

import com.mantas.chat.chatroom.Message;
import com.mantas.chat.user.User;

public class ReceivedMessageEvent {
    private final String roomName;
    private final Message message;

    public ReceivedMessageEvent(String roomName, Message message) {
        this.roomName = roomName;
        this.message = message;
    }

    public String getRoomName() {
        return roomName;
    }

    public Message getMessage() {
        return message;
    }
}
