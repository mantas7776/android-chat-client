package com.mantas.chat.chatroom;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.HashMap;

public class ChatRoomCache {
    HashMap<String, MutableLiveData<ChatRoom>> roomCache = new HashMap<>();

    public LiveData<ChatRoom> get(String roomName) {
        LiveData<ChatRoom> room = roomCache.get(roomName);
        return room;
    }

    public void set(ChatRoom room) {
        MutableLiveData<ChatRoom> cacheRoom = roomCache.get(room.getName());

        if (cacheRoom == null) {
            cacheRoom = new MutableLiveData<>();
            cacheRoom.setValue(room);
            roomCache.put(room.getName(), cacheRoom);
        } else {
            cacheRoom.setValue(room);
        }

    }

    public void remove(ChatRoom room) {
        roomCache.remove(room.getName());
    }

    public void remove(String roomName) {
        roomCache.remove(roomName);
    }

}
