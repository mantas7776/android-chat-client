package com.mantas.chat.chatroom;

import com.mantas.chat.chatroom.webResponses.ChatRoomsListResponse;
import com.mantas.chat.chatroom.webResponses.SuccessResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ChatRoomWebservice {
    @GET("rooms/my")
    Call<ChatRoomsListResponse> getMyRooms();

    @POST("rooms/join/{roomName}")
    Call<SuccessResponse> joinRoom(@Path("roomName") String roomName);

    @POST("rooms/leave/{room}")
    Call<SuccessResponse> leaveRoom(@Path("room") String room);

}
