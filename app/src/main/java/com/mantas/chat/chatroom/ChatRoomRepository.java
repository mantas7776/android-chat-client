package com.mantas.chat.chatroom;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Room;
import android.graphics.Color;
import android.util.Log;

import com.mantas.chat.chatroom.database.ChatRoomDao;
import com.mantas.chat.chatroom.events.ReceivedMessageEvent;
import com.mantas.chat.chatroom.events.SendMessageEvent;
import com.mantas.chat.chatroom.webResponses.ChatRoomResponse;
import com.mantas.chat.chatroom.webResponses.ChatRoomsListResponse;
import com.mantas.chat.chatroom.webResponses.SuccessResponse;
import com.mantas.chat.user.User;
import com.mantas.chat.user.UserWebservice;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


@Singleton
public class ChatRoomRepository {
    private static final String TAG = "ChatRoomRepository";
    private final ChatRoomCache roomsCache = new ChatRoomCache();
    //private final MutableLiveData<Set<String>> currentRooms = new MutableLiveData<>();
    private final ChatRoomDao chatRoomDao;
    private final EventBus eventBus;
    private final ChatRoomWebservice webservice;
    //private final Executor executor = Executors.newCachedThreadPool();

    @Inject
    public ChatRoomRepository(ChatRoomDao chatRoomDao, EventBus eventBus, Retrofit retrofit) {
        this.chatRoomDao = chatRoomDao;
        this.eventBus = eventBus;
        this.webservice = retrofit.create(ChatRoomWebservice.class);

        eventBus.register(this);
        //test
        //eventBus.post(new ReceivedMessageEvent("Test", new Message(new User(5, 0), "labas")));
    }

    public void load() {
        for( ChatRoom room : chatRoomDao.getAllRooms()) {
            roomsCache.set(room);
        }
        webservice.getMyRooms().enqueue(new Callback<ChatRoomsListResponse>() {
            @Override
            public void onResponse(Call<ChatRoomsListResponse> call, Response<ChatRoomsListResponse> response) {
                ChatRoomsListResponse body = response.body();
                for (ChatRoomResponse roomResponse : body.getRooms()) {
                    ChatRoom room = new ChatRoom(roomResponse.getName());
                    roomsCache.set(room);
                    chatRoomDao.save(room);
                }
            }

            @Override
            public void onFailure(Call<ChatRoomsListResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    public LiveData<ChatRoom> getRoom(String roomName) {
        LiveData<ChatRoom> room = roomsCache.get(roomName);
        if (room == null) throw new RuntimeException("Room does not exist or you do not have permission to access it!");
        return room;
    }

    public void set(ChatRoom room) {
        roomsCache.set(room);
    }

    public boolean joinRoom(String name) {
        webservice.joinRoom(name).enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                ChatRoom room = new ChatRoom(name);
                chatRoomDao.save(room);
                roomsCache.set(room);
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
        return true;
    }


    public boolean leaveRoom(String roomName) {
        ChatRoom room = getRoom(roomName).getValue();
        webservice.leaveRoom(roomName).enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                SuccessResponse body = response.body();
                if (body.isSuccess()) {
                    roomsCache.remove(room);
                    chatRoomDao.delete(room);
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
        return true;
    }

    public List<ChatRoom> getJoinedRooms() {
        return chatRoomDao.getAllRooms();
    }

    public LiveData<List<ChatRoom>> getJoinedRoomsLive() {
        return chatRoomDao.getAllRoomsLive();
    }

    public void sendMessage(String roomName, String messageText) {
        messageText = messageText.trim();
        eventBus.post(new SendMessageEvent(messageText, roomName));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceivedMessage(ReceivedMessageEvent event) {
        try {
            ChatRoom room = getRoom(event.getRoomName()).getValue();
            room.addMessage(event.getMessage());
            roomsCache.set(room);
        } catch(RuntimeException e) {
            Log.w(TAG, "onReceivedMessage: ", e);
        }
    }
}
