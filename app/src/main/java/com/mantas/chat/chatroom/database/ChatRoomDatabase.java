package com.mantas.chat.chatroom.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.mantas.chat.chatroom.ChatRoom;

@Database(entities = {ChatRoom.class}, version = 2)
public abstract class ChatRoomDatabase extends RoomDatabase {
    public abstract ChatRoomDao ChatRoomDao();
}