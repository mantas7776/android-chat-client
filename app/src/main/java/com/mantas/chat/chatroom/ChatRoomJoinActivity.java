package com.mantas.chat.chatroom;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.mantas.chat.MyApplication;
import com.mantas.chat.R;

public class ChatRoomJoinActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_room);
    }

    public void join(View view) {
        TextView textView = findViewById(R.id.join_room_text);
        String roomName = textView.getText().toString();
        ((MyApplication)getApplication()).getAppComponent().getChatRepository().joinRoom(roomName);
        finish();
    }

}
