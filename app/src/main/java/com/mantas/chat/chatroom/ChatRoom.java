package com.mantas.chat.chatroom;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.mantas.chat.user.User;

import java.util.ArrayList;

@Entity
public class ChatRoom {
    @PrimaryKey
    @NonNull
    private String name;
    //private int creator;
    @Ignore
    private ArrayList<Message> messages;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //public int getCreator() {
    //    return creator;
    //}

    //public void setCreator(int creator) {
    //    this.creator = creator;
    //}

    public ArrayList<Message> getMessages() {
        return messages;
    }

//    public ChatRoom(String name, int creator) {
//        this.name = name;
//        this.creator = creator;
//    }

    public ChatRoom(String name) {
        this.name = name;
        messages = new ArrayList<>();
    }

    public void addMessage(String text, User user) {
        messages.add(new Message(user, text));
    }
    public void addMessage(Message message) {
        messages.add(message);
    }
}
