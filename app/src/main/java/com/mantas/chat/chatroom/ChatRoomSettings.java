package com.mantas.chat.chatroom;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;

import com.mantas.chat.MainActivity;
import com.mantas.chat.MyApplication;
import com.mantas.chat.R;

public class ChatRoomSettings extends Activity {
    private String roomName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room_settings2);
        setTitle("Settings");

        Intent intent = getIntent();
        roomName = intent.getStringExtra(MainActivity.CHATROOM);
    }

    public void leave(View view) {
        ChatRoomRepository repo = ((MyApplication) getApplication()).getAppComponent().getChatRepository();
        repo.leaveRoom(repo.getRoom(roomName).getValue().getName());
        finish();
    }

}
