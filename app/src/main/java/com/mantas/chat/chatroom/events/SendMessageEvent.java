package com.mantas.chat.chatroom.events;

public class SendMessageEvent {
    public final String message;
    public final String roomName;

    public SendMessageEvent(String message, String roomName) {
        this.message = message;
        this.roomName = roomName;
    }
}
