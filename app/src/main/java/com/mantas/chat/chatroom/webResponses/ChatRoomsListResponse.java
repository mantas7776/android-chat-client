package com.mantas.chat.chatroom.webResponses;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.mantas.chat.chatroom.ChatRoom;

import java.util.List;

public class ChatRoomsListResponse {
    private List<ChatRoomResponse> rooms;
    private boolean success;

    public List<ChatRoomResponse> getRooms() {
        return rooms;
    }

    public boolean isSuccessful() {
        return success;
    }

}
