package com.mantas.chat.chatroom;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

public class ChatRoomListViewModel extends ViewModel {
    private ChatRoomRepository repo;
    private LiveData<List<ChatRoom>> rooms;

    @Inject
    public ChatRoomListViewModel(ChatRoomRepository repo) {
        this.repo = repo;
        this.rooms = repo.getJoinedRoomsLive();
    }

    public LiveData<List<ChatRoom>> getJoinedRooms() {
        return rooms;
    }

    public void joinRoom(String roomName) { repo.joinRoom(roomName); }
}
