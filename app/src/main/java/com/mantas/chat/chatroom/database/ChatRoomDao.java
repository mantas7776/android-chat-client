package com.mantas.chat.chatroom.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mantas.chat.chatroom.ChatRoom;

import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ChatRoomDao {
    @Insert(onConflict = REPLACE)
    void save(ChatRoom chatRoom);
    @Query("SELECT * FROM chatroom")
    LiveData<List<ChatRoom>> getAllRoomsLive();
    @Query("SELECT * FROM chatroom")
    List<ChatRoom> getAllRooms();
    @Delete
    void delete(ChatRoom chatRoom);
}