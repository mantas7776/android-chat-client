package com.mantas.chat.chatroom;


import android.app.Application;
import android.arch.persistence.room.Room;

import com.mantas.chat.chatroom.database.ChatRoomDao;
import com.mantas.chat.chatroom.database.ChatRoomDatabase;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ChatModule {

    private Application application;

    public ChatModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication() { return application; }

    @Provides
    @Singleton
    ChatRoomRepository provideRepository(ChatRoomDatabase db, EventBus eventBus, Retrofit retrofit) {
        return new ChatRoomRepository(db.ChatRoomDao(), eventBus, retrofit);
    }

    @Provides
    @Singleton
    ChatRoomDatabase provideDatabase(Application app) {
        return Room.databaseBuilder(app.getApplicationContext(),
                ChatRoomDatabase.class, "chat-database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return EventBus.builder().build();
    }


//    @Provides
//    @Singleton
//    ChatRoomDao provideChatRoomDao() {
//        return new ChatRoomDao();
//    }
}
