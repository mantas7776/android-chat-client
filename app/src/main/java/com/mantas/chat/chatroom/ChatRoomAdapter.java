package com.mantas.chat.chatroom;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mantas.chat.R;

public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.ViewHolder> {

    private static final String TAG = "ChatRoomAdapter";
    private ChatRoomViewModel viewModel;
    private ChatRoom room;


    public void setChatRoom(ChatRoom room) {
        this.room = room;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        Log.d(TAG, "onBindView: message created");
        Message message = room.getMessages().get(i);
        holder.message.setText(message.getText());
        GradientDrawable drawable = (GradientDrawable) holder.avatarCircle.getBackground();
        String hexColor = "#" + message.getUser().getColor();
        drawable.setColor(Color.parseColor(hexColor));
    }

    @Override
    public int getItemCount() {
        if (room == null) return 0;
        return room.getMessages().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View avatarCircle;
        TextView message;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            avatarCircle = itemView.findViewById(R.id.avatar);
            message = itemView.findViewById(R.id.message_body);

        }
    }
}
