package com.mantas.chat;

import com.mantas.chat.MyApplication;
import com.mantas.chat.chatroom.ChatModule;
import com.mantas.chat.chatroom.ChatRoomRepository;
import com.mantas.chat.dependencyInjection.ViewModelFactory;
import com.mantas.chat.dependencyInjection.ViewModelModule;
import com.mantas.chat.user.UserRepository;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;

@Singleton
@Component(modules = {ChatModule.class, ViewModelModule.class, AppModule.class})
public interface AppComponent {
    ViewModelFactory getViewModelFactory();
    EventBus getEventBus();
    UserRepository getUserRepository();
    ChatRoomRepository getChatRepository();
    OkHttpClient getOkHttpClient();

    //void inject(MyApplication application);
}
