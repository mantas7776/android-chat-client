package com.mantas.chat.dependencyInjection;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.mantas.chat.chatroom.ChatRoomListViewModel;
import com.mantas.chat.chatroom.ChatRoomViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory viewModelFactory);
    //You are able to declare ViewModelProvider.Factory dependency in another module. For example in ApplicationModule.

    @Binds
    @IntoMap
    @ViewModelKey(ChatRoomViewModel.class)
    abstract ViewModel provideChatRoomViewModel(ChatRoomViewModel chatRoomViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ChatRoomListViewModel.class)
    abstract ViewModel provideChatRoomListViewModel(ChatRoomListViewModel chatRoomListViewModel);
}